const gJsonCars = `[{"make":"Ford","model":"Explorer","year":2017,"color":"white","vID":"AB-10111"},
          {"make":"Toyota","model":"Corolla","year":2018,"color":"silver","vID":"DN-23218"},
          {"make":"Mazda","model":"Mazda 6","year":2020,"color":"red","vID":"TZ-23212"},
          {"make":"Toyota","model":"Fortuna","year":2016,"color":"black","vID":"IN-91925"},
          {"make":"Mazda","model":"Mazda 3","year":2019,"color":"silver","vID":"MN-44593"}]`;
//Parse json to object
const gCarObj = JSON.parse(gJsonCars);
//Check new car
const statusCar = (paramYear) => { return paramYear >= 2018 ? "Xe mới" : "Xe cũ" };
function App() {
  return (
    <div>
      <ul>
        {gCarObj.map((element, index) => <li key={index}>Tên: {element.make} Biển số: {element.vID} Tình trạng xe: {statusCar(element.year)}</li>)} 
      </ul>
      
    </div>
  );
}

export default App;
